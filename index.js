// alert("Hello World!");

// ITEM 1

let inputString1 = "webmaster";
console.log(`Example string: ${inputString1}`);

function sortStringFunction(inputString) {
	
	let stringVal = inputString.split("").sort().join("");

	return console.log(`Expected output: ${stringVal}`);

};

sortStringFunction(inputString1);



/*==================================================================================*/

// ITEM 2
console.log(" ");
let inputString2 = "The quick brown Fox";

function getNumberOfCharacters(inputString2) {

	let stringVal = "";
	for(let vowels = 0; vowels < inputString2.length; vowels++){
		if (
			inputString2[vowels].toLowerCase() == "a" ||
			inputString2[vowels].toLowerCase() == "e" ||
			inputString2[vowels].toLowerCase() == "i" ||
			inputString2[vowels].toLowerCase() == "o" ||
			inputString2[vowels].toLowerCase() == "u"
			) {

			stringVal += inputString2[vowels];
			
		}else{

			continue
		}
	}

	console.log("Example string: " + inputString2);
	console.log("Expected Output " + stringVal.length);

};


getNumberOfCharacters(inputString2);


/*==================================================================================*/

// ITEM 3
console.log(" ");
let coutries = ["Philippines","Indonesia","Vietman","Thailand"];

function addCountry(coutries = []) {
	coutries.unshift("Japan", "Singapore", "China: ");
	let arrayCountry = coutries;
	console.log("After Inserting New Elements At The Beginning of The Array: ");
	console.log(arrayCountry);
	console.log(" ");
	console.log("After Sorting All The Elements: ");
	console.log(arrayCountry.sort());
}
console.log("Before Adding Elements");
console.log(coutries);
console.log(" ");
addCountry(coutries);



/*==================================================================================*/

// ITEM 4
console.log(" ");


let person = {
	firstName: "Rico",
	lastName: "Tasong",
	gender: "Male",
	age: 28,
	nationality: "Filipino",

	introduce: function() {
		console.log(`Hello! My name is ${this.firstName} ${this.lastName}, ${this.age} years of age and a ${this.gender}. My nationality is ${this.nationality}.`);
	}
};

console.log(person);
person.introduce();

/*==================================================================================*/

// ITEM 5
console.log(" ");

function person1(name, type, specialMove, dream, attitude) {
	this.name = name
	this.type = type
	this.specialMove = specialMove
	this.dream = dream
	this.attitude = attitude


	this.person1troduction = function() {
		console.log(`${this.name} is a ${this.type} and his goal is to become ${this.dream}. He's special technique is called ${this.specialMove}. He is a ${this.attitude} person.`)
	} 
}

let ninja = new person1("Naruto", "nija", "Rasinggan", "Hokage", "stuborn");
console.log(ninja);	
ninja.person1troduction();

let pirate = new person1("Luffy", "pirate", "Gumo-Gumo no Red hawk", "Pirate King", "stuborn");
console.log(pirate);	
pirate.person1troduction();


let hero = new person1("Deku", "Hero", "United State of Word Smash", "Number 1 hero", "stuborn");
console.log(hero);	
hero.person1troduction();


/*console.log(" ");

function person2(name, type, specialMove, dream, attitude) {
	this.name = name
	this.type = type
	this.specialMove = specialMove
	this.dream = dream
	this.attitude


	this.person2troduction = function() {
		console.log(`${this.name} is a ${this.type} and his goal is to become ${this.dream}. He's special technique is called ${this.specialMove}.`)
	} 
}

let pirate = new person2("Luffy", "pirate", "Gumo-Gumo no Red hawk", "Pirate King");
console.log(pirate);	
pirate.person2troduction();
*/